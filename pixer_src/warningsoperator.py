import bmesh
import bpy
import functools

from .verticeschecker import has_duplicates, are_vertices_aligned_to_grid


def in_5_seconds(pixer):
    pixer.all_ok = False


class WarningsOperator(bpy.types.Operator):
    bl_label = "PixerWarnings"
    bl_idname = "rabid.pixerwarnings"

    def execute(self, context):
        scene = context.scene
        pixer = scene.pixer

        obj = context.active_object
        me = obj.data
        bm = bmesh.from_edit_mesh(me)

        pixer.grid_pixels_check = context.space_data.overlay.grid_subdivisions != pixer.pixels_in_3D_unit
        pixer.check_duplicated_vertices = has_duplicates(bm)
        pixer.check_aligned_vertices = not are_vertices_aligned_to_grid(bm, context)
        pixer.all_ok = not pixer.grid_pixels_check and not \
            pixer.check_duplicated_vertices and not \
            pixer.check_aligned_vertices
        if pixer.all_ok:
            bpy.app.timers.register(functools.partial(in_5_seconds, pixer), first_interval=5)
        return {'FINISHED'}
