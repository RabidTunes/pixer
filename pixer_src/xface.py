from __future__ import annotations

from typing import List, Tuple

from .geometryutils import are_perpendicular, are_parallel
from .logger import *
from .utils import *
from mathutils import Matrix, Vector
from math import radians
from bmesh.types import BMFace


#################
# XFACE CLASS ##
#################
# This class is just a wrapper for the default blender BMFace class to add
# some sugar on it
class XFace:
    # Static
    UP_VEC: Vector = Vector((0.0, 0.0, 1.0))
    DOWN_VEC: Vector = -UP_VEC
    MIN_VERTICAL_ANGLE: float = 30.0
    LATERAL: int = 0
    TOP: int = 1
    DOWN: int = 2
    ALL_XFACES = {}

    UV_LAYER = None
    FOLLOW_NEAR_ALIGNMENT: bool = False
    USE_SEAMS: bool = False

    @staticmethod
    def init(uv_layer, follow_near_alignment: bool, use_seams_as_pixel_align: bool):
        XFace.UV_LAYER = uv_layer
        XFace.FOLLOW_NEAR_ALIGNMENT = follow_near_alignment
        XFace.USE_SEAMS = use_seams_as_pixel_align

    # Variables of each object
    face: BMFace = None
    solved: bool = False
    plane = None
    horizontal_edges = []
    vertical_edges = []
    up: Vector = None

    def __init__(self, face: BMFace):
        log(DEBUG, "Creating XFace for " + str(face.index), ["init"])
        self.face = face
        self.solved = False
        self.horizontal_edges = []
        self.vertical_edges = []
        self._calculate_plane()
        self._calculate_edges_alignment()
        self._calculate_up()

        XFace.ALL_XFACES[face] = self

    def get_face(self) -> BMFace:
        return self.face

    def get_length(self) -> int:
        return len(self.face.loops)

    def get_index(self, index) -> int:
        while index < 0:
            index += self.get_length()
        return index % self.get_length()

    def get_plane(self) -> int:
        return self.plane

    def get_plane_string(self) -> str:
        if self.plane == XFace.LATERAL:
            return "LATERAL"
        elif self.plane == XFace.TOP:
            return "TOP"
        else:
            return "DOWN"

    def get_vertex(self, index: int) -> Vector:
        index = self.get_index(index)
        return self.face.loops[index].vert.co.copy()

    def get_edge(self, index: int) -> Vector:
        return self.get_vertex(self.get_index(index + 1)) - self.get_vertex(self.get_index(index))

    def get_score(self) -> float:
        return len(self.get_solved_neighbors()) + (len(self.horizontal_edges) + len(self.vertical_edges)) \
               / len(self.face.loops)

    def get_horizontal_edges(self) -> List[int]:
        return self.horizontal_edges

    def get_vertical_edges(self) -> List[int]:
        return self.vertical_edges

    def is_edge_aligned(self, edge_index):
        edge_index = self.get_index(edge_index)
        return edge_index in self.get_horizontal_edges() or edge_index in self.get_vertical_edges()

    def get_uv(self, index: int) -> Vector:
        return self.face.loops[self.get_index(index)][XFace.UV_LAYER].uv.copy()

    def get_all_uvs(self) -> List[Vector]:
        all_uvs = []
        for i in range(self.get_length()):
            all_uvs.append(self.get_uv(i))
        return all_uvs

    def get_uv_edge(self, index: int) -> Vector:
        return self.get_uv(index + 1) - self.get_uv(index)

    def update_uv(self, index: int, new_uv: Vector):
        index = self.get_index(index)
        self.face.loops[index][XFace.UV_LAYER].uv = new_uv

    def solve(self):
        self.solved = True

    def is_solved(self) -> bool:
        return self.solved

    def get_linked_xfaces_by_vertex(self) -> List[XFace]:
        linked_faces = [f for e in self.face.verts for f in e.link_faces if f is not self.face]
        linked_xfaces = []
        for linked_face in linked_faces:
            if linked_face not in XFace.ALL_XFACES:
                XFace.ALL_XFACES[linked_face] = XFace(linked_face)
            linked_xfaces.append(XFace.ALL_XFACES[linked_face])
        return linked_xfaces

    def get_linked_xfaces_by_edge(self):
        linked_xfaces_by_edge = []
        linked_xfaces = set()
        linked_xfaces.update(self.get_linked_xfaces_by_vertex())
        for linked_xface in linked_xfaces:
            common_edges = self.get_common_edges(linked_xface)
            for edge_tuple in common_edges:
                self_edge = edge_tuple[0]
                other_edge = edge_tuple[1]
                linked_xfaces_by_edge.append((self_edge, other_edge, linked_xface))
        return linked_xfaces_by_edge

    def has_any_aligned_edges(self) -> bool:
        return bool(self.horizontal_edges or self.vertical_edges)

    def get_solved_neighbors(self, on_aligned_edges=False) -> List[XFace]:
        solved_neighbors_by_edge = []
        solved_neighbors = [linked_xface for linked_xface in self.get_linked_xfaces_by_vertex() if linked_xface.is_solved()
                            and linked_xface.get_plane() == self.get_plane()]
        for neighbor in solved_neighbors:
            common_edges = self.get_common_edges(neighbor)
            for edge_tuple in common_edges:
                self_edge = edge_tuple[0]
                if not on_aligned_edges or (self_edge in self.get_horizontal_edges()) \
                        or (self_edge in self.get_vertical_edges()):
                    solved_neighbors_by_edge.append(neighbor)
        return solved_neighbors_by_edge

    def has_any_solved_neighbor(self, on_aligned_edges=False) -> bool:
        return bool(self.get_solved_neighbors_by_edge(on_aligned_edges))

    def get_solved_neighbors_by_edge(self, on_aligned_edges=False) -> List[Tuple[int, XFace]]:
        solved_neighbors_by_edge = []
        solved_neighbors = self.get_solved_neighbors()
        for neighbor in solved_neighbors:
            common_edges = self.get_common_edges(neighbor)
            for edge_tuple in common_edges:
                self_edge = edge_tuple[0]
                if not on_aligned_edges or (self_edge in self.get_horizontal_edges()) \
                        or (self_edge in self.get_vertical_edges()):
                    solved_neighbors_by_edge.append((self_edge, neighbor))
        return solved_neighbors_by_edge

    def is_inverted_against(self, other: XFace) -> bool:
        common_edges = self.get_common_edges(other)
        for common_edge in common_edges:
            self_edge = self.get_edge(common_edge[0]).normalized()
            other_edge = other.get_edge(common_edge[1]).normalized()
            self_basis_edge = self.get_basis_converted_edge(common_edge[0]).normalized()
            other_basis_edge = other.get_basis_converted_edge(common_edge[1]).normalized()

            if equals_sign(self_edge, -other_edge) and equals_sign(self_basis_edge, other_basis_edge):
                return True
        return False

    def get_common_vertices(self, other: XFace):
        log(DEBUG, "Finding common vertices between " + str(self.get_face().index) + " and "
            + str(other.get_face().index), ["matching"])
        common_vertices = []
        for i in range(self.get_length()):
            for j in range(other.get_length()):
                if self.get_vertex(i) == other.get_vertex(j):
                    common_vertices.append((i, j))

        log(DEBUG, "Common vertices\n" + str(common_vertices) + "\n", ["matching"])
        for v in common_vertices:
            log(DEBUG, str(v) + "///" + str(self.get_vertex(v[0])), ["matching"])
            log(DEBUG, str(v) + "///" + str(other.get_vertex(v[1])), ["matching"])
            log(DEBUG, "###", ["matching"])
        return common_vertices

    def get_common_edges(self, other: XFace):
        common_edges = []
        common_vertices = self.get_common_vertices(other)
        self_vertices = []
        other_vertices = []
        for vertex_tuple in common_vertices:
            self_vertices.append(vertex_tuple[0])
            other_vertices.append(vertex_tuple[1])

        for vertex_tuple in common_vertices:
            if self.get_index(vertex_tuple[0] + 1) in self_vertices:
                common_edges.append((vertex_tuple[0], other.get_index(vertex_tuple[1] - 1)))

        for v in common_edges:
            log(DEBUG, str(v) + "///" + str(self.get_edge(v[0])), ["matching"])
            log(DEBUG, str(v) + "///" + str(other.get_edge(v[1])), ["matching"])
            log(DEBUG, "###", ["matching"])
        return common_edges

    def get_basis_converted_vertex(self, index) -> Vector:
        normal = self.face.normal.normalized()
        basis_ihat = self.up.normalized().cross(normal).normalized()
        basis_jhat = normal.cross(basis_ihat).normalized()
        basis_khat = normal

        basis = Matrix().to_3x3()
        basis[0][0], basis[1][0], basis[2][0] = basis_ihat[0], basis_ihat[1], basis_ihat[2]
        basis[0][1], basis[1][1], basis[2][1] = basis_jhat[0], basis_jhat[1], basis_jhat[2]
        basis[0][2], basis[1][2], basis[2][2] = basis_khat[0], basis_khat[1], basis_khat[2]

        inv_basis = basis.inverted()
        return inv_basis @ self.get_vertex(index)

    def get_basis_converted_edge(self, index) -> Vector:
        return self.get_basis_converted_vertex(index + 1) - self.get_basis_converted_vertex(index)

    def get_edge_seams(self) -> List[int]:
        seams = []
        for i in range(self.get_length()):
            if self.face.loops[i].edge.seam:
                seams.append(i)
        return seams

    def has_seams(self) -> bool:
        return bool(self.get_edge_seams())

    def _calculate_plane(self):
        if self.face.normal.angle(XFace.UP_VEC) <= radians(XFace.MIN_VERTICAL_ANGLE):
            self.plane = XFace.TOP
        elif self.face.normal.angle(XFace.DOWN_VEC) <= radians(XFace.MIN_VERTICAL_ANGLE):
            self.plane = XFace.DOWN
        else:
            self.plane = XFace.LATERAL
        log(DEBUG, "Plane for " + str(self.face.index) + " is " + str(self.get_plane_string()), ["init"])

    def recalculate_alignment_and_up_by_seams(self, selected_only: bool = False):
        if XFace.USE_SEAMS and (not selected_only or self.get_face().select) and self.has_seams():
            self._recalculate_alignment_using_seams()

    def recalculate_alignment_and_up_by_near_aligned(self, selected_only: bool = False):
        if XFace.FOLLOW_NEAR_ALIGNMENT:
            self._recalculate_alignment_using_near_aligned(selected_only)

    def _recalculate_alignment_using_seams(self):
        seams = self.get_edge_seams()
        if len(seams) == 1:
            self._recalculate_alignment_and_up_vector_using_edge(seams[0], True)

    def _recalculate_alignment_using_near_aligned(self, selected_only: bool = False):
        if self.get_vertical_edges() or self.get_horizontal_edges():
            return
        linked_xfaces_by_edge = list(filter(lambda x: not selected_only or x.get_face().select,
                                            self.get_linked_xfaces_by_edge()))
        linked_xfaces_by_edge_aligned = []
        for linked_xface_tuple in linked_xfaces_by_edge:
            other_edge = linked_xface_tuple[1]
            linked_xface = linked_xface_tuple[2]
            if linked_xface.is_edge_aligned(other_edge):
                linked_xfaces_by_edge_aligned.append(linked_xface_tuple)
        if not linked_xfaces_by_edge_aligned:
            return
        if len(linked_xfaces_by_edge) == len(linked_xfaces_by_edge_aligned) or len(linked_xfaces_by_edge_aligned) == 1:
            self._recalculate_alignment_and_up_vector_with(linked_xfaces_by_edge_aligned[0])
            return
        for linked_xface_tuple in linked_xfaces_by_edge:
            self_edge_index = linked_xface_tuple[0]
            edge = self.get_edge(self_edge_index)
            for sub_linked_xface_tuple in linked_xfaces_by_edge:
                sub_self_edge_index = sub_linked_xface_tuple[0]
                if self_edge_index == sub_self_edge_index:
                    continue
                sub_edge = self.get_edge(sub_self_edge_index)
                if not are_parallel(edge, sub_edge) and not are_perpendicular(edge, sub_edge):
                    return

        # If we reached this point it means that all edges with aligned neighbors are parallel or perpendicular between
        # themselves so it is safe to recalculate using the first neighbor
        self._recalculate_alignment_and_up_vector_with(linked_xfaces_by_edge_aligned[0])

    def _recalculate_alignment_and_up_vector_with(self, other: Tuple[int, int, XFace]):
        self_edge = other[0]
        other_edge = other[1]
        other_xface = other[2]
        self._recalculate_alignment_and_up_vector_using_edge(self_edge,
                                                             other_edge in other_xface.get_horizontal_edges())

    def _recalculate_alignment_and_up_vector_using_edge(self, edge: int, is_horizontal: bool):
        self.horizontal_edges = []
        self.vertical_edges = []
        if is_horizontal:
            self.horizontal_edges.append(edge)
        else:
            self.vertical_edges.append(edge)

        for sub_edge in range(self.get_length()):
            if sub_edge == edge:
                continue
            if are_parallel(self.get_edge(sub_edge), self.get_edge(edge)):
                if is_horizontal:
                    self.horizontal_edges.append(sub_edge)
                else:
                    self.vertical_edges.append(sub_edge)
            if are_perpendicular(self.get_edge(sub_edge), self.get_edge(edge)):
                if is_horizontal:
                    self.vertical_edges.append(sub_edge)
                else:
                    self.horizontal_edges.append(sub_edge)

        self.up = self.get_edge(edge).normalized().cross(self.face.normal.normalized()).normalized()

    def _calculate_edges_alignment(self):
        self._face_normal_edge_calculation()
        self._log_edge_alignment()

    def _face_normal_edge_calculation(self):
        if self.plane == XFace.LATERAL:
            for i in range(len(self.face.loops)):
                curr_v = self.get_vertex(i)
                next_v = self.get_vertex(i + 1)
                if curr_v.z == next_v.z:
                    self.horizontal_edges.append(i)
                if curr_v.x == next_v.x and curr_v.y == next_v.y:
                    self.vertical_edges.append(i)
        else:
            for i in range(len(self.face.loops)):
                curr_v = self.get_vertex(i)
                next_v = self.get_vertex(i + 1)
                if curr_v.y == next_v.y:
                    self.horizontal_edges.append(i)
                if curr_v.x == next_v.x:
                    self.vertical_edges.append(i)

    def _log_edge_alignment(self):
        log(DEBUG, "Horizontal edges in face " + str(self.face.index) + " are " + str(self.horizontal_edges), ["init"])
        log(DEBUG, "Vertical edges in face " + str(self.face.index) + " are " + str(self.vertical_edges), ["init"])

    def _calculate_up(self):
        if self.plane == XFace.LATERAL:
            self.up = Vector((0.0, 0.0, 1.0))
        elif self.plane == XFace.TOP:
            self.up = Vector((0.0, 1.0, 0.0))
        else:
            self.up = Vector((0.0, -1.0, 0.0))

    def __eq__(self, other):
        if other is None:
            return False
        return self.get_face() == other.get_face()

    def __hash__(self):
        return self.get_face().__hash__()

    def __ne__(self, other):
        if other is None:
            return True
        return self.get_face() != other.get_face()

    def __str__(self):
        return "XFACE(" + str(self.face.index) + ", " + self.get_plane_string() + ")"
